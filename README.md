# UniRepo

Welcome to my university repository dedicated to share materials and programs I used or created during my studies.
Each semester will be in a separated branch. I am also using materials from the older projects, so that is why some commits will be larger as I copied all my files in one commit.

Links:
* https://courses.fit.cvut.cz/
* https://progtest.fit.cvut.cz/
